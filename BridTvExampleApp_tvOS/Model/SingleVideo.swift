//
//  Company.swift
//  BridTVWhiteLabel
//
//  Created by Xcode Peca on 26/11/2020.
//

import Foundation

struct SingleVideo: Codable {
    
    var Player: Player
    var Related: [Video]
    
}
