//
//  Channel.swift
//  BridTVWhiteLabel
//
//  Created by Xcode Peca on 26/11/2020.
//

import Foundation

struct PlaylistData: Codable {
    
    var Player: Player
    var Video: [Video]
}

