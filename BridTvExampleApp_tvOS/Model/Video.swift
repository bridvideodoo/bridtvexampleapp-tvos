//
//  Videos.swift
//  BridTVWhiteLabel
//
//  Created by Xcode Peca on 26/11/2020.
//

import Foundation

struct Video: Codable {
    
    var id: String?
    var name: String?
    var description: String?
    var duration: String?
    var image: String?
    var snapshots: SnapshotWrapper?
}
