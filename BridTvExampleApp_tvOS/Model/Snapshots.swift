//
//  Snapshot.swift
//  BridTvExampleApp_tvOS
//
//  Created by Predrag Jevtic on 30.3.22..
//

import Foundation

class Snapshots: Codable {
    var hsd: String?
    var sd: String?
    var th: String?
    var ld: String?
    var hd: String?
    var fhd: String?
    
}

enum SnapshotWrapper: Codable
{
    case value(Snapshots)
    case error
    
    init(from decoder: Decoder) throws {
        if let snapshots = try? decoder.singleValueContainer().decode(Snapshots.self) {
            self = .value(snapshots)
        } else {
            self = .error
        }
    }
}
