//
//  Playlist.swift
//  BridTVExample
//
//  Created by Predrag Jevtic on 20.7.22..
//

import Foundation

struct Playlist: Codable {

    var playerId: Int?
    var type: String?
    var items: Int?
    var id: Int?
    var name: String?
}
