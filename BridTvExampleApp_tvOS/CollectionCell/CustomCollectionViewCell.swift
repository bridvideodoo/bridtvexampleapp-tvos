//
//  CustomCollectionViewCell.swift
//  BridTvExampleApp_tvOS
//
//  Created by Predrag Jevtic on 4.2.22..
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageThumb: UIImageView!
    @IBOutlet weak var descriptionText: UILabel!
    
    static let identifier = "CustomCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "CustomCollectionViewCell", bundle: nil)
    }
    
    var models = [Video]()
    var didFocusItemAction: ((UIImage) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageThumb.layer.cornerRadius = 20
        imageThumb.clipsToBounds = true
        imageThumb.layer.borderWidth = 5
        imageThumb.layer.borderColor = UIColor.clear.cgColor
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func commonInit() {
        
        self.layoutIfNeeded()
        self.layoutSubviews()
        self.setNeedsDisplay()
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        
        coordinator.addCoordinatedAnimations({
            self.transform = self.isFocused ? CGAffineTransform( scaleX:1.06, y:1.06) : .identity
        }, completion: nil)
        
        if (self.isFocused) {
            self.imageThumb.layer.borderWidth = 2
            self.imageThumb.layer.borderColor = UIColor.white.cgColor
            self.descriptionText.alpha = 1
        } else {
            self.imageThumb.layer.borderColor = UIColor.clear.cgColor
            self.descriptionText.alpha = 0.5
        }
    }
    
    public func configure(with model:Video, models:[Video]) {
        if let name = model.name, let image = model.image {
            self.models = models
            self.descriptionText.text = name
            self.imageThumb.downloaded(from: image)
        }
    }
}
