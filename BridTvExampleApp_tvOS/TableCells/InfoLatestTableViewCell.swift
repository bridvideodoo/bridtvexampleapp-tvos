//
//  InfoLatestTableViewCell.swift
//  BridTvExampleApp_tvOS
//
//  Created by Predrag Jevtic on 2.2.22..
//

import UIKit

class InfoLatestTableViewCell: UITableViewCell {

    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var imageThumb: UIImageView!
    
    
    var didSelectItemAction: ((Video) -> Void)?
    
    var playerId: String?
    var videoItem: Video?
    
    static let identifier = "InfoLatestTableViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "InfoLatestTableViewCell", bundle: nil)
    }
    
    public func configure(with model:Video, playerId:String, videoItem:Video) {
        
        if let name = model.name {
            self.titleText.text = name
            self.playerId = playerId
            self.videoItem = videoItem
        }
    }
    
    @IBAction func play(_ sender: Any) {
        didSelectItemAction?(videoItem!)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    

}
