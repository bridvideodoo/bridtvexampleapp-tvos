//
//  CollectionLatestTableViewCell.swift
//  BridTvExampleApp_tvOS
//
//  Created by Predrag Jevtic on 2.2.22..
//

import UIKit

class CustomCollectionTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var apiRequest = ApiRequest()
    //    var models = [Video]()
    var playerID: String?
    var playlist: Playlist?
    var didSelectItemAction: ((Video) -> Void)?
    var didFocusItem: ((Video) -> Void)?
    var page: Int = 1
    var isPageRefreshing:Bool = false
    
    var models: [Video]! {
        didSet {
            isPageRefreshing = false
            collectionView.reloadData()
        }
    }
    
    static let identifier = "CustomCollectionTableViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "CustomCollectionTableViewCell", bundle: nil)
    }
    
    func configure(with models:[Video], playerID:String, playlist: Playlist) {
        self.playerID = playerID
        self.models = models
        self.playlist = playlist
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
        if !(models.count < 1) {
            collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: true)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(CustomCollectionViewCell.nib(), forCellWithReuseIdentifier: CustomCollectionViewCell.identifier)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCollectionViewCell.identifier, for: indexPath) as! CustomCollectionViewCell
        cell.configure(with: models[indexPath.row], models: models)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if collectionView == self.collectionView {
            if let index = context.nextFocusedIndexPath?.row {
                didFocusItem?(models[index])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 500, height: 390)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelectItemAction?(models[indexPath.row])
    }
    
}
