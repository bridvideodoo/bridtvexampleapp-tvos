//
//  InfoViewController.swift
//  BridTvExampleApp_tvOS
//
//  Created by Predrag Jevtic on 7.2.22..
//

import UIKit
import BridSDKtvOS

class InfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var playerId: String?
    var apiRequest = ApiRequest()
    var video: Video?
    var sendVideoData: Video?
    var player: BVPlayer?
    var playerController: AVPlayerViewController?
    var imageRendition = ImageRendition()
    var playlist: Playlist?
    
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBAction func playVideo(_ sender: Any) {
        player = nil
        self.playerController = AVPlayerViewController.init()
        if let playerId = playerId, let videoId = video?.id {
            player = BVPlayer(data: BVData(playerID: Int32(playerId)!, forVideoID: Int32(videoId)!), forAvController: self.playerController)!
            self.present(self.playerController!, animated: true, completion: nil)
        }
    }
    
    var related: SingleVideo! {
        didSet {
            tableView.register(CustomCollectionTableViewCell.nib(), forCellReuseIdentifier: CustomCollectionTableViewCell.identifier)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionText.isScrollEnabled = false
        descriptionText.textContainerInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        if let text = video?.name, let time = video?.duration {
            self.titleText.text = "  \(text) "
            self.durationLabel.text = "\(secondsToHoursMinutesSeconds(time))"
        }
        self.descriptionText.text = video?.description
        self.mainImage.downloaded(from: (video?.image)!)
        
        getRealtedVideo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        player = nil
    }
    
    func reloadViewController(video: Video) {
        descriptionText.isScrollEnabled = false
        descriptionText.textContainerInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        if let text = video.name, let time = video.duration {
            self.titleText.text = "  \(text) "
            self.durationLabel.text = "\(secondsToHoursMinutesSeconds(time))"
        }
        self.descriptionText.text = video.description
        changeImage(image: imageRendition.getBestImage(video: video))
        
        forceFocus(to: playButton)
        
        getRealtedVideo()
        
    }
    
    func getRealtedVideo() {
        if let videoId = video?.id {
            apiRequest.getRelatedVideo(playerID: playerId!, videoID: videoId) { (result) in
                switch result {
                case .success(let model):
                    self.related = model
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                }
            }
        }
    }
    
    func forceFocus(to: UIButton) {
        playButton = to
        setNeedsFocusUpdate()
        updateFocusIfNeeded()
    }
    
    override var preferredFocusEnvironments: [UIFocusEnvironment] {
        return [playButton!]
    }
    
    func changeImage(image :String) {
        UIView.transition(with: mainImage, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.mainImage.downloaded(from: image)
        }, completion: nil)
        
    }
    
    //MARK - Draw minutes from string
    func secondsToHoursMinutesSeconds(_ seconds: String) -> (String) {
        if let time = Int(seconds) {
            return time.convertSecondsToHrMinuteSec(seconds: time)
        }
        return ""
    }
    
    //MARK: - TableView Delegates
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let relatedCell = tableView.dequeueReusableCell(withIdentifier: CustomCollectionTableViewCell.identifier, for: indexPath) as! CustomCollectionTableViewCell
        relatedCell.configure(with: related.Related, playerID: related.Player.id, playlist: Playlist(playerId: 0, type: "Related", items: 0, id: 0, name: "related"))
        relatedCell.didSelectItemAction = { [weak self] video in
            self?.video = video
            self!.reloadViewController(video: self!.video!)
        }
        relatedCell.layoutIfNeeded()
        
        return relatedCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.bounds.height / 1.2
    }
    
}
