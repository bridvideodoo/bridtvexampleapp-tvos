//
//  ProbaViewController.swift
//  BridTvExampleApp_tvOS
//
//  Created by Predrag Jevtic on 26.7.22..
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var apiRequest = ApiRequest()
    var imageRendition = ImageRendition()
    var mainImage: UIImageView = UIImageView()
    var logo: UIImageView = UIImageView()
    var tableView: UITableView = UITableView()
    var titleName: UILabel = UILabel()
    var playlist: Playlist!
    var videosData: PlaylistData! {
        didSet {
            setupTableView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUIComponents()
        
        if videosData != nil {
            setupTableView()
        }
    }
    
    func setupTableView() {
        changeImage(image: imageRendition.getBestImage(video: videosData.Video[1]))
        tableView.register(CustomCollectionTableViewCell.nib(), forCellReuseIdentifier: CustomCollectionTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func changeImage(image :String) {
        UIView.transition(with: mainImage, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.mainImage.downloaded(from: image)
        }, completion: nil)
        
    }
    
    func setUIComponents() {
        
        mainImage = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        
        logo  = UIImageView(frame: CGRect(x: 110, y: 30, width: 200, height: 100))
        logo.contentMode = .scaleAspectFit
        logo.image = UIImage(named: "logo")
        
        tableView = UITableView(frame: CGRect(x: 0, y: 640, width: Int(self.view.frame.size.width), height: Int(self.view.frame.size.width)/4))
        tableView.isScrollEnabled = false
        
        
        self.view.addSubview(mainImage)
        
        self.view.addSubview(logo)
        self.view.addSubview(tableView)
        self.view.addSubview(titleName)
        
        titleName.translatesAutoresizingMaskIntoConstraints = false
        titleName.font = UIFont(name: "Helvetica-Bold", size: 45)
        view.addConstraint(NSLayoutConstraint(item: titleName, attribute: .leading, relatedBy: .equal, toItem: logo, attribute: .leading, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: titleName, attribute: .top, relatedBy: .equal, toItem: self.tableView, attribute: .top, multiplier: 1, constant: 20))
        
        setGradient()
    }
    
    func setGradient() {
        let logoGradient  = UIImageView(frame: CGRect(x: 0, y: 0, width: mainImage.frame.width, height: mainImage.frame.height))
        logoGradient.image = UIImage(named: "logoGradient")
        mainImage.addSubview(logoGradient)
        let bottomGradient = CAGradientLayer()
        let topColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        let bottomColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        bottomGradient.frame = CGRectMake(0, mainImage.frame.height - mainImage.frame.height * 0.5, mainImage.frame.width, mainImage.frame.height * 0.5)
        bottomGradient.colors = [topColor, bottomColor]
        mainImage.layer.insertSublayer(bottomGradient, at: 0)

    }
    
    //MARK: - TableView Delegates
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let channelCell = tableView.dequeueReusableCell(withIdentifier: CustomCollectionTableViewCell.identifier, for: indexPath) as! CustomCollectionTableViewCell
        channelCell.configure(with: videosData.Video, playerID: videosData.Player.id, playlist: playlist)
        channelCell.didSelectItemAction = { [weak self] video in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let infoVC = storyboard.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
            infoVC.video = video
            infoVC.playerId = self!.videosData.Player.id
            self!.present(infoVC, animated: true)
        }
        channelCell.didFocusItem = { [weak self] video in
            self!.changeImage(image: self!.imageRendition.getBestImage(video: video))
        }
        channelCell.layoutIfNeeded()
        return channelCell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.bounds.height
    }
    
}
