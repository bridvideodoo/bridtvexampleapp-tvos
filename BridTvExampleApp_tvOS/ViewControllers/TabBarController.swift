//
//  TabBarController.swift
//  BridTvExampleApp_tvOS
//
//  Created by Predrag Jevtic on 26.7.22..
//

import UIKit

class TabBarController: UITabBarController, PassingDataProtocol  {
    
    let apiRequest = ApiRequest()
    var playlists: [Playlist]?
    var playlistsData: [PlaylistData] = []
    
    func passDataForPlaylist(data: [PlaylistData]) {
        playlistsData = data
    }
    
    func passListName(playlistData: [Playlist]) {
        playlists = playlistData
    }
    
    func passControllers(controllers: [MainViewController]) {
        self.setViewControllers(controllers, animated: false)
        var index = 0
        for item in playlists! {
            controllers[index].playlist = item
            controllers[index].title = item.name
            controllers[index].titleName.text = item.name
            controllers[index].videosData = playlistsData[index]
            index += 1
        }
        
    }
    
    func loadConfigData() {
        apiRequest.getBuilder{ (result) in
            switch result {
            case .success(let model):
                self.apiRequest.config = model
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    func createTabBarControllers(contorllers: [MainViewController]) {
        for item in contorllers {
            self.setViewControllers([item], animated: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiRequest.delegate = self
        loadConfigData()
        
    }
    
}
