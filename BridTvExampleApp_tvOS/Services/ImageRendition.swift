//
//  ImageRendition.swift
//  BridTvExampleApp_tvOS
//
//  Created by Predrag Jevtic on 30.3.22..
//

import Foundation

class ImageRendition {
    
    func getBestImage(video: Video) -> String {
        
        if let image = video.snapshots {
            switch image {
            case .value(let snapshots):
                if let fhd = snapshots.fhd {
                    return fhd
                } else  if let hd = snapshots.hd {
                    return hd
                } else if let hsd = snapshots.hsd {
                        return hsd
                } else if let sd = snapshots.sd {
                    return sd
                } else if let ld = snapshots.ld {
                    return ld
                } else if let th = snapshots.th {
                    return th
                }
            case .error:
                if video.image != nil {
                    return video.image!
                }
            }
        }
        
        return ""
    }
}
