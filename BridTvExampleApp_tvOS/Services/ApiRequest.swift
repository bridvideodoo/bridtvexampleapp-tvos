//
//  ApiRequest.swift
//  Example App
//
//  Created by Predrag Jevtic on 21.1.22..
//

import Foundation

class ApiRequest {
    
    //   //MARK: - Setting local varibale
    
    let referenceUrl = "https://services.brid.tv/services/get/"
    var playlistsURls: [String] = []
    var playlistsData: [PlaylistData] = []
    var delegate: PassingDataProtocol?
    var tabBarControllers: [MainViewController] = []
    private var dataTask: URLSessionDataTask?
    var config: Config! {
        didSet {
            self.createUrlFromData(playlists: config.tv!.playlist)
        }
    }
    
    //MARK: - Methods for JSON Data
    
    func getBuilder(completition: @escaping (Result<Config, Error >) -> Void) {
        guard let url = Bundle.main.url(forResource: "config", withExtension: "json") else { return }
        
        if let JSONData = try? Data(contentsOf: url) {
            do {
                let myModel = try! JSONDecoder().decode(Config.self, from: JSONData)
                DispatchQueue.main.async {
                    completition(.success(myModel))
                }
            }
        }
    }
    
    func createUrlFromData(playlists: [Playlist]) {
        for playlist in playlists {
            if let playerType = playlist.type, let  playerId = playlist.playerId, let playlistId = playlist.id {
                createUrl(type: playerType, playerId: "\(playerId)", playlistId: "\(playlistId)")
                let controller = MainViewController()
                tabBarControllers.append(controller)
            }
        }
        getPlaylistData()
    }
    
    func createUrl(type: String, playerId: String, playlistId: String) {
        let createdUrl = "\(referenceUrl)\(type)/\(playerId)/\(playlistId)/1/25/0.json"
        playlistsURls.append(createdUrl)
    }
    
    func getPlaylistData() {
        fetchURLs(playlistsURls) {
            self.delegate?.passListName(playlistData: self.config.tv!.playlist)
            self.delegate?.passDataForPlaylist(data: self.playlistsData)
            self.delegate?.passControllers(controllers: self.tabBarControllers)
        }
    }
    
    func fetchURLs(_ initialURLs: [String], completionHandler: @escaping () -> Void) {
        var pendingURLs = initialURLs
        func startNext() {
            guard let urlString = pendingURLs.first else {
                completionHandler()
                return
            }
            pendingURLs.removeFirst()
            guard let url = URL(string: urlString) else { return }
            getJSON(url: url,  completition: { (result) in
                switch result {
                case .success(let model):
                    self.playlistsData.append(model)
                    startNext()
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                }
            })
        }
        startNext()
        
        if pendingURLs.isEmpty {
            completionHandler()
        }
    }
    
    //MARK: - Paging
    
    func getRelatedNextPage(type: String, playerID: String, playlistId: Int, page: Int, completition: @escaping (Result<PlaylistData, Error >) -> Void) {
        guard let url = URL(string: "\(referenceUrl)\(type)/\(playerID)/\(playlistId)/\(page)/5/0.json") else { return }
        
        if let JSONData = try? Data(contentsOf: url) {
            do {
                let myModel = try! JSONDecoder().decode(PlaylistData.self, from: JSONData)
                DispatchQueue.main.async {
                    completition(.success(myModel))
                }
            }
        }
    }
    
    //MARK: - Parsing JSON
    
    func getJSON(url:URL, completition: @escaping (Result<PlaylistData, Error >) -> Void) {
        
        if let JSONData = try? Data(contentsOf: url) {
            do {
                let myModel = try! JSONDecoder().decode(PlaylistData.self, from: JSONData)
                DispatchQueue.main.async {
                    completition(.success(myModel))
                }
            }
        }
    }
    
    //MARK: - Related
    
    func getRelatedVideo(playerID: String, videoID: String, completition: @escaping (Result<SingleVideo, Error >) -> Void) {
        guard let url = URL(string: "https://services.brid.tv/services/get/video/\(playerID)/\(videoID).json") else { return }
        
        if let JSONData = try? Data(contentsOf: url) {
            do {
                let myModel = try! JSONDecoder().decode(SingleVideo.self, from: JSONData)
                DispatchQueue.main.async {
                    completition(.success(myModel))
                }
            }
        }
    }
}

protocol PassingDataProtocol {
    func passDataForPlaylist(data: [PlaylistData])
    func passListName(playlistData: [Playlist])
    func passControllers(controllers: [MainViewController])
}



