//
//  ExtensionHelperClass.swift
//  Example App
//
//  Created by Predrag Jevtic on 21.1.22..
//

import UIKit

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFill) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFill) {
        var linkToURl: String?
        if !(link.hasPrefix("http:")) && !(link.hasPrefix("https:")) {
            linkToURl = String(format: "https:%@", link)
        } else { linkToURl = link }
        
        guard let url = URL(string: linkToURl!) else { return }
        downloaded(from: url, contentMode: mode)
    }
    
}


extension Int {
    
    func convertSecondsToHrMinuteSec(seconds:Int) -> String{
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .abbreviated
        
        let formattedString = formatter.string(from:TimeInterval(seconds))!
        print(formattedString)
        return formattedString
    }
    
    func secondsToTime() -> String {
        
        let (m,s) = ((self % 3600) / 60, (self % 3600) % 60)
        let m_string =  m < 10 ? "0\(m)" : "\(m)"
        let s_string =  s < 10 ? "0\(s)" : "\(s)"
        
        return "\(m_string):\(s_string)"
    }
}
